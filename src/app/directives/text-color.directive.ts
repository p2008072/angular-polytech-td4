import {Directive, ElementRef, Input, OnChanges, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[appTextColor]'
})
export class TextColorDirective implements OnChanges {
  @Input() appTextColor!: string;

  constructor(private el: ElementRef) {}
  ngOnChanges(changes: SimpleChanges) {
    switch (this.appTextColor){
      case 'STANDARD':{
        this.el.nativeElement.style.color = 'blue'
        break;
      }
      case 'PREMIUM':{
        this.el.nativeElement.style.color = 'red'
        break;
      }
      case 'BUSINESS':{
        this.el.nativeElement.style.color = 'green'
        break;
      }
    }
  }
}
