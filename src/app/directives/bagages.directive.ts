import {Directive, ElementRef, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Passager} from "../models/passager.model";

@Directive({
  selector: '[appBagages]'
})
export class BagagesDirective implements OnChanges{
@Input() appBagages!:Passager
  constructor(private el: ElementRef) {}
  ngOnChanges(changes: SimpleChanges) {
    switch (this.appBagages.classeVol){
      case 'STANDARD':{
        this.el.nativeElement.style.backgroundColor = this.appBagages.nbBagagesSoute>1?'red':'transparent'
        break;
      }
      case 'PREMIUM':{
        this.el.nativeElement.style.backgroundColor = this.appBagages.nbBagagesSoute>2?'red':'transparent'
        break;
      }
      case 'BUSINESS':{
        this.el.nativeElement.style.backgroundColor = this.appBagages.nbBagagesSoute>3?'red':'transparent'
        break;
      }
    }
  }
}
