import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Vol} from "../../models/vol.model";

@Component({
  selector: 'app-liste-vols',
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent{
  @Input() listeVols!: Vol[];
  @Input() type!: "atterrissages"|"decollages"
  @Output() volEmitter:EventEmitter<Vol> = new EventEmitter<Vol>();

  onClick(vol: Vol):void {
    this.volEmitter.emit(vol);
  }
}
