import {Component, OnDestroy, OnInit} from '@angular/core';
import { IFiltres } from 'src/app/models/filtres.model';
import { Vol } from 'src/app/models/vol.model';
import { VolService } from '../../services/vol.service';
import {Subscription} from "rxjs";
import {PassagerService} from "../../services/passager.service";
import {IPassagerDto, Passager} from "../../models/passager.model";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-view-airfrance',
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent implements OnDestroy, OnInit{

  vols: Vol[] = [];
  vol!: Vol;
  listePassagers:Passager[]= []
  type!:"atterrissages"|"decollages";
  private subscribtions: Subscription[] = [];

  constructor(
    private _volService: VolService,
    private _passagerService: PassagerService,
    private _activatedRoute: ActivatedRoute) { }

  /**
   * Réaction à la mise à jour des filtres
   * On souhaite récupérer les vols qui correspondent aux filtres passés en paramètre
   * en utilisant le service défini dans le constructeur
   * @param filtres récupérés depuis le composant enfant
   */
  onFiltresEvent(filtres: IFiltres): void {
    if(this.type === "decollages") {
      this.subscribtions.push(this._volService.getVolsDepart(filtres.aeroport.icao, Math.round(filtres.debut.getTime() / 1000), Math.round(filtres.fin.getTime() / 1000)).subscribe(element => {
        this.vols = element;
      }));
    }
    else{
      this.subscribtions.push(this._volService.getVolsArrivee(filtres.aeroport.icao, Math.round(filtres.debut.getTime() / 1000), Math.round(filtres.fin.getTime() / 1000)).subscribe(element => {
        this.vols = element;
      }));
    }
  }

  onVol(vol:Vol) {
    this.vol = vol
    this.subscribtions.push(this._passagerService.getPassagers(this.vol.icao).subscribe((element => {
      this.listePassagers = element;
    })));
  }

  ngOnInit(): void {
    this.subscribtions.push(this._activatedRoute.data.subscribe((data$) => {
      this.type = data$['type'] ? data$['type'] : 'decollages';
    }))
  }

  ngOnDestroy() {
   this.subscribtions.forEach((element:Subscription)=>{
     element.unsubscribe();
    })
  }
}
