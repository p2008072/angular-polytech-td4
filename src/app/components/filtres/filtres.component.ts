import {Component, EventEmitter, Output, ViewEncapsulation} from '@angular/core';
import { MAT_DATE_RANGE_SELECTION_STRATEGY } from '@angular/material/datepicker';
import { FiveDayRangeSelectionStrategy } from 'src/app/date-adapter';
import { IAeroport } from 'src/app/models/aeroport.model';
import { IFiltres } from "../../models/filtres.model";
import { AEROPORTS } from '../../constants/aeroport.constant';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-filtres',
  templateUrl: './filtres.component.html',
  styleUrls: ['./filtres.component.scss'],
  providers: [
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: FiveDayRangeSelectionStrategy,
    },
  ],
  encapsulation: ViewEncapsulation.None
})
export class FiltresComponent {
  @Output() dataEvent = new EventEmitter<any>();
  /**
   * La liste des aéroports disponibles est une constante,
   * on n'utilise que les principaux aéroports français pour l'instant
   */
  aeroports: IAeroport[] = AEROPORTS;

  form:FormGroup<any> = new FormGroup({
    range: new FormGroup(
    {
      start:new FormControl('',[Validators.required]),
      end:new FormControl('',[Validators.required])
    }),
    airport: new FormControl('',[Validators.required])
  });

  onClick(){
    let result = this.form.getRawValue();
    let filtre: IFiltres = { aeroport: result.airport, debut: result.range.start, fin: result.range.end }
    this.dataEvent.emit(filtre)
  }
}
