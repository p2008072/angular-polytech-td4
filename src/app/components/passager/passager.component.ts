import { Component, Input } from '@angular/core';
import { Passager } from "../../models/passager.model";
import { TextColorDirective } from "../../directives/text-color.directive";

@Component({
  selector: 'app-passager',
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.scss']
})
export class PassagerComponent {
  @Input() passager!: Passager;
  @Input() photoDisplay!: boolean;
}
