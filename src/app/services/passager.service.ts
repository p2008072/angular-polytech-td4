import { Injectable } from '@angular/core';
import {IPassagerDto, Passager} from "../models/passager.model";
import {ajax} from "rxjs/internal/ajax/ajax";
import {elementAt, map, Observable} from "rxjs";
import {AjaxResponse} from "rxjs/internal/ajax/AjaxResponse";

@Injectable({
  providedIn: 'root'
})
export class PassagerService {

  constructor() { }

  getPassagers(seed:string):Observable<any>{
    return ajax(`https://randomuser.me/api/?results=20&seed=${seed}`)
      .pipe(map((element:any)=>element.response.results))
      .pipe(map((element:any)=>{
        return element.map((passager:any):Passager => {
          return new Passager({name:passager.name,picture:passager.picture,email:passager.email});
        })
      }));
  }
}
